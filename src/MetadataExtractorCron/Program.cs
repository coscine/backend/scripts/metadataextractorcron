﻿using MetadataExtractorCron.Extractors;
using MetadataExtractorCron.Models.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using NLog;
using Winton.Extensions.Configuration.Consul;

var configBuilder = new ConfigurationBuilder();

var consulUrl = Environment.GetEnvironmentVariable("CONSUL_URL") ?? "http://localhost:8500";

var configuration = configBuilder
    .AddConsul(
        "coscine/Coscine.Infrastructure/MetadataExtractorCron/appsettings",
        options =>
        {
            options.ConsulConfigurationOptions =
                cco => cco.Address = new Uri(consulUrl);
            options.Optional = true;
            options.ReloadOnChange = true;
            options.PollWaitTime = TimeSpan.FromSeconds(5);
            options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
        }
    )
    .AddEnvironmentVariables()
    .Build();

var metadataExtractorConfiguration = new MetadataExtractorConfiguration();
configuration.Bind(MetadataExtractorConfiguration.Section, metadataExtractorConfiguration);

// Set the default LogLevel
LogManager.Configuration.Variables["logLevel"] = metadataExtractorConfiguration.Logger?.LogLevel ?? "Trace";
// Set the log location
LogManager.Configuration.Variables["logHome"] = metadataExtractorConfiguration.Logger?.LogHome ?? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");

var logger = LogManager.GetCurrentClassLogger();

if (!metadataExtractorConfiguration.IsEnabled)
{
    logger.Info("Disabled, skipped run.");
    return;
}

try
{
    var metadataExtractor = new CoscineMetadataExtractor(metadataExtractorConfiguration);

    await metadataExtractor.PerformExtraction();
}
catch (Exception e)
{
    logger.Info("Aborted on error.");
    logger.Error(e);
    return;
}

logger.Info("Finished successfully.");
