﻿using VDS.RDF;
using MetadataExtractorCron.Util;
using VDS.RDF.Parsing;
using System.Globalization;
using MetadataExtractorCron.Models.ConfigurationModels;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using NLog;
using MetadataExtractor.Util;
using VDS.RDF.Writing;
using Coscine.MetadataExtractor.Core.Model;
using Coscine.MetadataExtractor.Core.Api;

namespace MetadataExtractorCron.Extractors;

public class CoscineMetadataExtractor : IMetadataExtractor
{
    private readonly string _resourceUrlPrefix = "https://purl.org/coscine/resources";

    private readonly MetadataExtractorConfiguration _metadataExtractorConfiguration;

    private readonly DefaultApi _extractionApiClient;

    private readonly string _adminToken;
    protected readonly Configuration _apiConfiguration;
    private readonly AdminApi _adminApi;
    private readonly BlobApi _blobApi;
    private readonly TreeApi _treeApi;

    private readonly Logger _logger = LogManager.GetCurrentClassLogger();

    public CoscineMetadataExtractor(MetadataExtractorConfiguration metadataExtractorConfiguration)
    {
        _metadataExtractorConfiguration = metadataExtractorConfiguration;

        _extractionApiClient = new DefaultApi(metadataExtractorConfiguration.MetadataExtractionUrl);

        // Generate an admin token for the API Client
        var jwtConfiguration = ApiConfigurationUtil.RetrieveJwtConfiguration();
        _adminToken = ApiConfigurationUtil.GenerateAdminToken(jwtConfiguration);
        _apiConfiguration = new Configuration()
        {
            BasePath = "http://localhost:7206/coscine",
            ApiKeyPrefix = { { "Authorization", "Bearer" } },
            ApiKey = { { "Authorization", _adminToken } },
            Timeout = 300000, // 5 minutes
        };
        _adminApi = new AdminApi(_apiConfiguration);
        _blobApi = new BlobApi(_apiConfiguration);
        _treeApi = new TreeApi(_apiConfiguration);
    }

    public async Task PerformExtraction()
    {
        var resourcesAsyncList = PaginationHelper.GetAllAsync<ResourceAdminDtoPagedResponse, ResourceAdminDto>(
            (currentPage) => _adminApi.GetAllResourcesAsync(includeDeleted: false, pageNumber: currentPage, pageSize: 50)
        );

        await foreach (var resource in resourcesAsyncList)
        {
            var projectId = resource.ProjectResources.FirstOrDefault()?.ProjectId;
            if (!resource.MetadataExtraction || !projectId.HasValue)
            {
                continue;
            }
            string projectIdString = projectId.Value.ToString();

            var resourceId = resource.Id;

            _logger.Info($"Working on resource {resourceId}");

            var fileInfos = await ReceiveAllFiles(projectIdString, resourceId);

            foreach (var file in fileInfos.Where((fileInfo) => fileInfo.Type == TreeDataType.Leaf))
            {
                if (file.Size > _metadataExtractorConfiguration.DetectionByteLimit)
                {
                    _logger.Info($"Skipping {file.Path} on {resourceId} since it has a too large byte size");
                    continue;
                }
                _logger.Info($"Iterating over {file.Path} on {resourceId}");
                if (!await HasCurrentMetadataExtracted(projectIdString, resourceId, file))
                {
                    _logger.Info($"Extracting metadata for {file.Path} on {resourceId}");
                    try
                    {
                        var extractedMetadata = await ExtractMetadata(projectIdString, resourceId, file);
                        await StoreExtractedMetadata(projectIdString, resourceId, file, extractedMetadata);
                    }
                    catch (Exception e)
                    {
                        _logger.Info($"Error extracting metadata for {file.Path} on {resourceId} with error message: {e.Message}, Inner: {(e.InnerException != null ? e.InnerException.Message : "none")}");
                    }
                }
                else
                {
                    _logger.Info($"Metadata for {file.Path} on {resourceId} already exists");
                }
            }
        }
    }

    private async Task<IEnumerable<FileTreeDto>> ReceiveAllFiles(string projectId, Guid resourceId, string path = "")
    {
        var fileInfos = new List<FileTreeDto>();

        var currentFileInfos = await PaginationHelper.GetAll<FileTreeDtoPagedResponse, FileTreeDto>(
            (currentPage) => _treeApi.GetFileTreeAsync(projectId, resourceId, path, pageNumber: currentPage, pageSize: 50)
        );
        fileInfos.AddRange(currentFileInfos);
        foreach (var currentFileInfo in currentFileInfos.Where(
            (currentFileInfo) => currentFileInfo.Type == TreeDataType.Tree
                // Deal with things like the ".coscine" folder
                && currentFileInfo.Hidden == false
        ))
        {
            fileInfos.AddRange(await ReceiveAllFiles(projectId, resourceId, currentFileInfo.Path));
        }
        return fileInfos;
    }

    private async Task<bool> HasCurrentMetadataExtracted(string projectId, Guid resourceId, FileTreeDto entry)
    {
        var metadataTreeResponse = await _treeApi.GetSpecificMetadataTreeAsync(projectId, resourceId, entry.Path, includeExtractedMetadata: true);
        if (
            metadataTreeResponse is not null
            && metadataTreeResponse.Data.AvailableVersions.Count != 0
            && metadataTreeResponse.Data.Extracted is not null
            && metadataTreeResponse.Data.Extracted.Definition is not null
        )
        {
            var version = VersionUtil.GetVersion(metadataTreeResponse.Data.Extracted.MetadataId) + "";
            if (version == metadataTreeResponse.Data.VarVersion)
            {
                return true;
            }
        }

        return false;
    }

    private async Task<MetadataOutput> ExtractMetadata(string projectId, Guid resourceId, FileTreeDto entry)
    {
        var loadedEntry = await _blobApi.GetBlobAsync(projectId, resourceId, entry.Path)
            ?? throw new NullReferenceException("The resulting stream of the loaded entry is null.");

        var tempFile = Path.Combine(Path.GetTempPath(), Path.GetFileName(entry.Path));
        using (var fs = new FileStream(tempFile, FileMode.Create, FileAccess.Write))
        {
            loadedEntry.CopyTo(fs);
        }

        var givenStream = File.OpenRead(tempFile);
        try
        {
            var extractedOutputs = await _extractionApiClient.PostMetadataExtractorWorkerAsync(
                "application/json",
                identifier: $"{resourceId}/{entry.Path.Replace("\\", "/")}",
                creationDate: entry.CreationDate?.ToString("o", CultureInfo.InvariantCulture)!,
                modificationDate: entry.ChangeDate?.ToString("o", CultureInfo.InvariantCulture)!,
                file: givenStream
            );

            return extractedOutputs[0];
        }
        finally
        {
            givenStream.Dispose();
            File.Delete(tempFile);
        }
    }

    private async Task StoreExtractedMetadata(string projectId, Guid resourceId, FileTreeDto entry, MetadataOutput extractedMetadata)
    {
        var metadataExtractorVersion = (await _extractionApiClient.GetVersionWorkerAsync()).VarVersion;

        var resourceGraphName = $"{_resourceUrlPrefix}/{resourceId}";
        var newFileGraphName = $"{resourceGraphName}/{entry.Path}";
        var newFileGraphNameAddon = newFileGraphName;
        if (!newFileGraphNameAddon.EndsWith('/'))
        {
            newFileGraphNameAddon += "/";
        }

        var metadataTreeResponse = await _treeApi.GetSpecificMetadataTreeAsync(projectId, resourceId, entry.Path);

        var recentMetadataVersion = metadataTreeResponse?.Data.Id;
        var recentDataVersion = recentMetadataVersion?.Replace("type=metadata", "type=data");

        var possibleNewVersion = VersionUtil.GetNewVersion();
        var possibleNewDataVersion = $"{newFileGraphNameAddon}@type=data&version={possibleNewVersion}";
        var possibleNewMetadataVersion = $"{newFileGraphNameAddon}@type=metadata&version={possibleNewVersion}";

        var hashValue = await CreateHashData(projectId, resourceId, entry);

        var correctMetadataVersion = (recentMetadataVersion ?? possibleNewMetadataVersion);

        var recentDataExtractedVersion = new Uri((recentDataVersion ?? possibleNewDataVersion) + "&extracted=true");
        var recentMetadataExtractedVersion = new Uri(correctMetadataVersion + "&extracted=true");

        var tripleStore = new TripleStore();
        tripleStore.LoadFromString(extractedMetadata.Metadata, new TriGParser(TriGSyntax.Rdf11));

        var trigWriter = new TriGWriter();

        FormatResultMetadata(tripleStore, recentDataExtractedVersion, recentMetadataExtractedVersion);

        if (recentMetadataVersion is null)
        {
            await _treeApi.CreateExtractedMetadataTreeAsync(
                projectId,
                resourceId,
                new ExtractedMetadataTreeForCreationDto(
                    path: entry.Path,
                    id: correctMetadataVersion,
                    definition: new RdfDefinitionForManipulationDto(
                        VDS.RDF.Writing.StringWriter.Write(tripleStore, trigWriter),
                        RdfFormat.ApplicationXTrig
                    ),
                    provenance: new ProvenanceParametersDto(
                        metadataExtractorVersion: metadataExtractorVersion,
                        hashParameters: new HashParametersDto(
                            HashUtil.DefaultHashAlgorithm.Name ?? "SHA512",
                            hashValue
                        )
                    )
                )
            );
        }
        else
        {
            await _treeApi.UpdateExtractedMetadataTreeAsync(
                projectId,
                resourceId,
                new ExtractedMetadataTreeForUpdateDto(
                    path: entry.Path,
                    id: correctMetadataVersion,
                    definition: new RdfDefinitionForManipulationDto(
                        VDS.RDF.Writing.StringWriter.Write(tripleStore, trigWriter),
                        RdfFormat.ApplicationXTrig
                    ),
                    provenance: new ProvenanceParametersDto(
                        metadataExtractorVersion: metadataExtractorVersion,
                        hashParameters: new HashParametersDto(
                            HashUtil.DefaultHashAlgorithm.Name ?? "SHA512",
                            hashValue
                        )
                    )
                )
            );
        }
    }

    private async Task<string> CreateHashData(string projectId, Guid resourceId, FileTreeDto entry)
    {
        var loadedEntry = await _blobApi.GetBlobAsync(projectId, resourceId, entry.Path)
            ?? throw new NullReferenceException("The resulting stream of the loaded entry is null, when trying to hash the data.");

        return Convert.ToBase64String(HashUtil.HashData(loadedEntry));
    }

    private static void FormatResultMetadata(TripleStore tripleStore, Uri dataExtractGraph, Uri metadataExtractGraph)
    {
        foreach (var graph in tripleStore.Graphs.ToArray())
        {
            if (graph.Name is IUriNode)
            {
                var graphName = (IUriNode)graph.Name;
                if (graphName.Uri.AbsoluteUri != dataExtractGraph.AbsoluteUri && graphName.Uri.AbsoluteUri != metadataExtractGraph.AbsoluteUri)
                {
                    tripleStore.Remove(graph.Name);
                    if (graphName.Uri.AbsoluteUri.Contains("type=data"))
                    {
                        var newGraph = new Graph(dataExtractGraph)
                        {
                            BaseUri = dataExtractGraph
                        };
                        newGraph.Assert(graph.Triples);
                        tripleStore.Add(newGraph, true);
                    }
                    else
                    {
                        var newGraph = new Graph(metadataExtractGraph)
                        {
                            BaseUri = metadataExtractGraph
                        };
                        newGraph.Assert(graph.Triples);
                        tripleStore.Add(newGraph, true);
                    }
                }
            }
        }
    }

}