﻿namespace MetadataExtractorCron.Extractors;

public interface IMetadataExtractor
{
    Task PerformExtraction();
}