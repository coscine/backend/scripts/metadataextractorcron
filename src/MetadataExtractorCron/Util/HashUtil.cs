﻿using System.Security.Cryptography;

namespace MetadataExtractorCron.Util
{
    /// <summary>
    /// Provides utility functions to calculate hashes of research data
    /// </summary>
    public static class HashUtil
    {
        /// <summary>
        /// The default hash algorithm being used (SHA512)
        /// </summary>
        public static HashAlgorithmName DefaultHashAlgorithm => HashAlgorithmName.SHA512;

        private static HashAlgorithm GetHashAlgorithm(HashAlgorithmName hashAlgorithmName)
        {
            if (hashAlgorithmName == HashAlgorithmName.MD5)
                return MD5.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA1)
                return SHA1.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA256)
                return SHA256.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA384)
                return SHA384.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA512)
                return SHA512.Create();

            throw new CryptographicException($"Unknown hash algorithm \"{hashAlgorithmName.Name}\".");
        }

        /// <summary>
        /// Takes a data stream and returns a hash represnetation of it
        /// </summary>
        /// <param name="data"></param>
        /// <param name="hashAlgorithmName"></param>
        /// <returns></returns>
        public static byte[] HashData(Stream data, HashAlgorithmName? hashAlgorithmName = null)
        {
            hashAlgorithmName ??= DefaultHashAlgorithm;
            using var hashAlgorithmObject = GetHashAlgorithm(hashAlgorithmName.Value);
            return hashAlgorithmObject.ComputeHash(data);
        }
    }
}

