/*
 * Metadata Extractor API
 *
 * This API extracts RDF triples from files
 *
 * The version of the OpenAPI document: 0.4.3
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;

namespace Coscine.MetadataExtractor.Core.Client
{
    /// <summary>
    /// A delegate to ExceptionFactory method
    /// </summary>
    /// <param name="methodName">Method name</param>
    /// <param name="response">Response</param>
    /// <returns>Exceptions</returns>
    public delegate Exception ExceptionFactory(string methodName, IApiResponse response);
}
