# Coscine.MetadataExtractorCron - the C# cronjob for the Metadata Extractor API

[[_TOC_]] 

## 📝 Overview

This cronjob interacts with the Coscine Api and the Metadata Extraction API to extract metadata from research data.

### Features
- **Configuration-driven**: Behavior driven by settings defined in configuration files and environment variables (see `appsettings.json`).
- **Logging**: Extensive logging capabilities to track the process and troubleshoot issues.

## ⚙️ Configuration

The deployment script uses a configuration class `MetadataExtractorCronConfiguration` to manage settings such as:
- `IsEnabled`: Toggles the extractor on or off.
- `MetadataExtractionUrl`: Specifies the metadata extraction service URL.
- `DetectionByteLimit`: Specifies limit the metadata extraction cronjob should adhere to.
- `Logger` Configuration: Specifies the logging level and output directory.

### Example `appsettings.json`
```json
{
  "MetadataExtractorConfiguration": {
    "IsEnabled": true,
    "MetadataExtractionUrl": "https://metadataextractor.otc.coscine.dev/",
    "DetectionByteLimit": 16000000,
    "Logger": {
      "LogLevel": "Trace",
      "LogHome": "C:\\coscine\\logs\\MetadataExtractorCron\\"
    }
  }
}
```

## 📖 Usage

To get started with this project, you will need to ensure you have configured and built it correctly. 

1. Execute the built executable (`.exe`)

To use the **MetadataExtractorCron**, execute the main program with appropriate command-line arguments to control its operation.

## 👥 Contributing

As an open source platform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!
